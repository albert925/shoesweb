$(inicio_admscript)

function inicio_admscript () {
	$('#bcd').on('click', abrircaja)
	$('.doll').on('click', borrarc)
}

function abrircaja (e) {
	e.preventDefault()
	$('.caja').slideToggle()
}

function borrarc (e) {
	e.preventDefault()
	var id = $(this).attr('data-id')
	var ruta = $(this).attr('href')
	var comf = confirm("Estas seguro de eliminarlo?")
	if (comf == true) {
		$.post(ruta, {id: id}, resborr)
	}
}

function resborr (res) {
	console.log(res);
	var dat = JSON.parse(res)
	console.log(dat)
	if (res == '1') {
		alert('Sesion caducada')
	}
	else{
		if (dat.es == "2") {
			$('#cjac'+dat.id).animate({height: 0, width: 0, "overflow": "hidden"}, 500, terminanim(dat.id))
		}
		else{
			alert(res)
			console.log(res)
		}
	}
}

function terminanim (id) {
	$('#cjac'+id).css({"overflow": "hidden"})
}
