$(in_producto)

function in_producto () {
	$('#Nvcl').on('click', nuevo_cliente)
	$('#Nvtp').on('click', nuevo_tipo)
	$('#Nvmk').on('click', nuevo_marca)
	$('.camcl').on('click', mof_cliente)
	$('.camtp').on('click', mof_tipos)
	$('.cimmmk').on('click', mof_imgmk)
	$('.cammk').on('click', mof_lttmk)
}

var mal = {color:'#CD0000'}
var normal = {color:'#000'}
var bien = {color:'#37A2D8'}

function loading () {
	var load = '<div class="windows8">'
		load += '<div class="wBall" id="wBall_1">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
		load += '<div class="wBall" id="wBall_2">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
		load += '<div class="wBall" id="wBall_3">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
		load += '<div class="wBall" id="wBall_4">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
		load += '<div class="wBall" id="wBall_5">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
	load += '</div>'
	return load
}

function es_imagen (tipederf) {
	switch(tipederf.toLowerCase()){
		case 'jpg':
			return true
			break
		case 'gif':
			return true
			break
		case 'png':
			return true
			break
		case 'jpeg':
			return true
			break
		default:
			return false
			break
	}
}

function nuevo_cliente () {
	var cl = $('#ttcl').val()
	if (cl == '') {
		$('#txA').css(mal).text('Ingrese el tipo de cliente')
		return false
	}
	else{
		$('#txA').css(normal).text('')
			.prepend(loading)
		$.post('new_cliente.php', {a:cl}, resulG)
		return false
	}
}

function nuevo_tipo () {
	var ta = $('#cltp').val()
	var tb = $('#tttp').val()
	if (ta == '' || ta == '0') {
		$('#txA').css(mal).text('Selecione tipo cliente')
		return false
	}
	else{
		if (tb === '') {
			$('#txA').css(mal).text('Ingrese el nombre del tipo')
			$('#tttp').focus()
			return false
		}
		else{
			$('#txA').css(normal).text('')
				.prepend(loading)
			$.post('new_tipo.php', {a:ta, b:tb}, resulG)
			return false
		}
	}
}

function nuevo_marca () {
	var mka = $('#ttmk').val()
	var mkig = $('#igmk')[0].files[0]
	var namemkig = mkig.name
	var extemkig = namemkig.substring(namemkig.lastIndexOf('.')+1)
	var tammkig = mkig.size
	var tipomkig = mkig.type
	if (mka === '') {
		$('#txA').css(mal).text('Ingrese el nombre de la marca')
		$('#ttmk').focus()
		return false
	}
	else{
		if (!es_imagen(extemkig)) {
			$('#txA').css(mal).text('Tipo de imagen no permitido')
			return false
		}
		else{
			$('#txA').css(normal).text('')
			var formu = new FormData($('#sl_mk')[0])
			$.ajax({
				url: '../../../nuevomarca.php',
				type: 'POST',
				data: formu,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#txA").prepend(loading)
				},
				success:reulimg,
				error:function () {
					$("#txA").css(mal).text("Ocurrió un error")
					$("#txA").fadeIn();$("#txA").fadeOut(3000)
				}
			})
			return false
		}
	}
}

function resulG (res) {
	if (res == '2') {
		$('#txA').css(mal).text('El nombre ya existe')
		return false
	}
	else{
		if (res == '3') {
			$('#txA').css(bien).text('Ingresado')
			location.reload(20)
		}
		else{
			$('#txA').css(mal).html(res)
			return false
		}
	}
}

function mof_cliente () {
	var ida = $(this).attr('data-id')
	var tf = $('#ftcl_'+ida).val()
	$('#txC_'+ida).fadeIn()
	if (tf === '') {
		$('#txC_'+ida).css(mal).text('Ingrese la palabra')
	}
	else{
		$('#txC_'+ida).css(normal).text('')
			.prepend(loading)
		$.post('mofcliente.php', {id:ida, a:tf}, function (res) {
			if (res == '2') {
				$('#txC_'+ida).css(bien).text('Modificado')
					.fadeOut()
			}
			else{
				$('#txC_'+ida).css(mal).html(res)
			}
		})
	}
}

function mof_tipos () {
	var idb = $(this).attr('data-id')
	var clf = $('#fcltp_'+idb).val()
	var tpf = $('#fttp_'+idb).val()
	$('#txC_'+idb).fadeIn()
	if (clf == '0' || clf == '') {
		$('#txC_'+idb).css(mal).text('Selecione tipo de cliente')
	}
	else{
		if (tpf === '') {
			$('#txC_'+idb).css(mal).text('Ingrese la palabra')
		}
		else{
			$('#txC_'+idb).css(normal).text('')
				.prepend(loading)
			$.post('moftipo.php', {id:idb, a:clf, b:tpf}, function (res) {
				if (res == '2') {
					$('#txC_'+idb).css(bien).text('Modificado')
						.fadeOut()
				}
				else{
					$('#txC_'+idb).css(mal).html(res)
				}
			})
		}
	}
}

function mof_lttmk () {
	var ida = $(this).attr('data-id')
	var ca = $('#ftmk_'+ida).val()
	$('#txC_'+ida).fadeIn()
	if (ca === '') {
		$('#txC_'+ida).css(mal).text('ingrese el nombre de la marca')
	}
	else{
		$('#txC_'+ida).css(normal).text('')
			.prepend(loading)
		$.post('moflet_marka.php', {id:ida, a:ca}, function (res) {
			if (res == '2') {
				$('#txC_'+ida).css(bien).text('Nombre modificado')
					.fadeOut()
			}
			else{
				$('#txC_'+ida).css(mal).html(res)
			}
		})
	}
}

function reulimg (res) {
	if (res == '2') {
		$('#txA').css(mal).text('Carpeta sin permisos o resolución de imagen no permitido')
			.fadeIn()
			.fadeOut(3000)
		return false
	}
	else{
		if (res == '3') {
			$('#txA').css(mal).text('Tamaño no permitido')
				.fadeIn()
				.fadeOut(3000)
			return false
		}
		else{
			if (res == '4') {
				$('#txA').css(mal).text('Carpeta sin permisos o resolución de imagen no permitido')
					.fadeIn()
					.fadeOut(3000)
				return false
			}
			else{
				if (res == '5') {
					$('#txA').css(bien).text('Dato ingresado')
						.fadeIn()
						.fadeOut(3000)
					location.reload(20)
				}
				else{
					$('#txA').css(mal).html(res)
					$('#txA').fadeIn()
					return false
				}
			}
		}
	}
}

function mof_imgmk () {
	var idG = $(this).attr('data-id')
	var idin = $('#idf_'+idG).val()
	var fiig = $('#igmkff_'+idG)[0].files[0]
	var namefiig=fiig.name
	var extefiig=namefiig.substring(namefiig.lastIndexOf('.')+1)
	var tamfiig=fiig.size
	var tipofiig=fiig.type
	if (idin === '') {
		$('#txD_'+idG).css(mal).text('Id de marca no disponible')
		return false
	}
	else{
		if (!es_imagen(extefiig)) {
			$('#txD_'+idG).css(mal).text('Tipo de imagen no permitido')
			return false
		}
		else{
			$('#txD_'+idG).css(normal).text('')
			var formu = new FormData($('#mf_mk'+idG)[0])
			$.ajax({
				url: '../../../mofimgmarca.php',
				type: 'POST',
				data: formu,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#txD_"+idG).prepend(loading)
				},
				success:function (res) {
					resid(res, idG)
				},
				error:function () {
					$("txD_"+idG).css(mal).text("Ocurrió un error")
					$("txD_"+idG).fadeIn();$("txD_"+idG).fadeOut(3000)
				}
			})
			return false
		}
	}
}

function resid (res, id) {
	if (res == '2') {
		$('#txD_'+id).css(mal).text('Carpeta sin permisos o resolución de imagen no permitido')
			.fadeIn()
			.fadeOut(3000)
		return false
	}
	else{
		if (res == '3') {
			$('#txD_'+id).css(mal).text('Tamaño no permitido')
				.fadeIn()
				.fadeOut(3000)
			return false
		}
		else{
			if (res == '4') {
				$('#txD_'+id).css(mal).text('Carpeta sin permisos o resolución de imagen no permitido')
					.fadeIn()
					.fadeOut(3000)
				return false
			}
			else{
				if (res == '5') {
					$('#txD_'+id).css(bien).text('imagen cambiado')
						.fadeIn()
						.fadeOut(3000)
					location.reload(20)
				}
				else{
					$('#txD_'+id).css(mal).html(res)
					$('#txD_'+id).fadeIn()
					return false
				}
			}
		}
	}
}
