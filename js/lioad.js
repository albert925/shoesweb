$(in_ffdaad)

function in_ffdaad () {
	$('#biad').on('click', ingadm)
	$('#camA').on('click', cambus)
	$('#camB').on('click', cambps)
}

var mal = {color:'#CD0000'}
var normal = {color:'#000'}
var bien = {color:'#37A2D8'}

function loading () {
	var load = '<div class="windows8">'
		load += '<div class="wBall" id="wBall_1">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
		load += '<div class="wBall" id="wBall_2">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
		load += '<div class="wBall" id="wBall_3">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
		load += '<div class="wBall" id="wBall_4">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
		load += '<div class="wBall" id="wBall_5">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
	load += '</div>'
	return load
}

function ingadm () {
	var ha = $('#usad').val()
	var hb = $('#psad').val()
	if (ha == '') {
		$('#txA').css(mal).text('Ingrese el nombre de usuario')
		$('#usad').focus()
		return false
	}
	else{
		if (hb === '') {
			$('#txA').css(mal).text('Ingrese la contraseña')
			$('#psad').focus()
			return false
		}
		else{
			$('#txA').css(normal).text('')
				.prepend(loading)
			$.post('ingradm.php', {a:ha, b:hb}, resulA)
			return false
		}
	}
}

function resulA (res) {
	if (res == '2') {
		$('#txA').css(mal).text('Nombre de usuario o contraseña incorrectos')
		return false
	}
	else{
		if (res == '3') {
			$('#txA').css(bien).text('Ingresando..')
			window.location.href='administrador'
		}
		else{
			$('#txA').html(res)
		}
	}
}

function cambus () {
	var id = $(this).attr('data-id')
	var usf = $('#fusad').val()
	if (usf === '') {
		$('#txA').css(mal).text('Ingrese el nombre de usuario')
	}
	else{
		$('#txA').css(normal).text('')
			.prepend(loading)
		$.post('mofusad.php', {id:id, a:usf}, resulB)
	}
}

function resulB (res) {
	if (res == '2') {
		$('#txA').css(mal).text('Nombre de usuario ya existe')
	}
	else{
		if (res == '3') {
			$('#txA').css(bien).text('Nombre de usuario cambiado')
			setTimeout(direcionar, 1500)
		}
		else{
			$('#txA').css(mal).html(res)
		}
	}
}

function cambps () {
	var idb = $(this).attr('data-id')
	var psa = $('#cac').val()
	var psb = $('#nna').val()
	var psc = $('#nnb').val()
	if (psa === '') {
		$('#txB').css(mal).text('Ingrese la contraseña actual')
		$('#cac').focus()
	}
	else{
		if (psb === '' || psb.length < 6) {
			$('#txB').css(mal).text('Contraseña nueva mínimo 6 dígitos')
			$('#nna').focus()
		}
		else{
			if (psc != psb) {
				$('#txB').css(mal).text('Las contraseñas nuevas no coinciden')
			}
			else{
				$('#txB').css(normal).text('')
					.prepend(loading)
				$.post('mfpsad.php', {id:idb, ac:psa, nv: psc}, resulC)
			}
		}
	}
}

function resulC (res) {
	if (res == '2') {
		$('#txB').css(mal).text('La contraseña actual es incorrecta')
	}
	else{
		if (res == '3') {
			$('#txB').css(bien).text('Contraseña cambiada')
			setTimeout(direcionar, 1500)
		}
		else{
			$('#txB').css(mal).html(res)
		}
	}
}

function direcionar () {
	window.location.href="../../cerrar"
}
