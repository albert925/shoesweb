$(in_noticias)

function in_noticias () {
	$('#nvnt').on('click', nueva_noticia)
	$('#mfig').on('click', mofimg_noticia)
	$('#mfnt').on('click', mofdat_noticia)
}

var mal = {color:'#CD0000'}
var normal = {color:'#000'}
var bien = {color:'#37A2D8'}

function loading () {
	var load = '<div class="windows8">'
		load += '<div class="wBall" id="wBall_1">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
		load += '<div class="wBall" id="wBall_2">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
		load += '<div class="wBall" id="wBall_3">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
		load += '<div class="wBall" id="wBall_4">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
		load += '<div class="wBall" id="wBall_5">'
			load += '<div class="wInnerBall"></div>'
		load += '</div>'
	load += '</div>'
	return load
}

function es_imagen (tipederf) {
	switch(tipederf.toLowerCase()){
		case 'jpg':
			return true
			break
		case 'gif':
			return true
			break
		case 'png':
			return true
			break
		case 'jpeg':
			return true
			break
		default:
			return false
			break
	}
}

function nueva_noticia () {
	var tt = $('#ttnt').val()
	var igt = $('#ignt')[0].files[0]
	var nameigt=igt.name
	var exteigt=nameigt.substring(nameigt.lastIndexOf('.')+1)
	var tamigt=igt.size
	var tipoigt=igt.type
	if (tt === '') {
		$('#txA').css(mal).text('Ingrese el título')
		$('#ttnt').focus()
		return false
	}
	else{
		if (!es_imagen(exteigt)) {
			$('#txA').css(mal).text('Tipo de imagen no permitido')
			return false
		}
		else{
			$('#txA').css(normal).text('')
				.prepend(loading())
			var formu = new FormData($('#sl_nt')[0])
			$.ajax({
				url: '../../../nuevonoticia.php',
				type: 'POST',
				data: formu,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#txA").prepend(loading)
				},
				success:reulimg,
				error:function () {
					$("#txA").css(mal).text("Ocurrió un error")
					$("#txA").fadeIn();$("#txA").fadeOut(3000)
				}
			})
			return false
		}
	}
}
function reulimg (res) {
	if (res == '2') {
		$('#txA').css(mal).text('Carpeta sin permisos o resolución de imagen no permitido')
			.fadeIn()
			.fadeOut(3000)
		return false
	}
	else{
		if (res == '3') {
			$('#txA').css(mal).text('Tamaño no permitido')
				.fadeIn()
				.fadeOut(3000)
			return false
		}
		else{
			if (res == '4') {
				$('#txA').css(mal).text('Carpeta sin permisos o resolución de imagen no permitido')
					.fadeIn()
					.fadeOut(3000)
				return false
			}
			else{
				if (res == '5') {
					$('#txA').css(bien).text('Noticia ingresada')
						.fadeIn()
						.fadeOut(3000)
					location.reload(20)
				}
				else{
					$('#txA').css(mal).html(res)
					$('#txA').fadeIn()
					return false
				}
			}
		}
	}
}

function mofimg_noticia () {
	var idin = $('#idc').val()
	var fiig = $('#ignt')[0].files[0]
	var namefiig=fiig.name
	var extefiig=namefiig.substring(namefiig.lastIndexOf('.')+1)
	var tamfiig=fiig.size
	var tipofiig=fiig.type
	if (idin == '' || idin == '0') {
		$('#txA').css(mal).text('Id noticia no disponible')
		return false
	}
	else{
		if (!es_imagen(extefiig)) {
			$('#txA').css(mal).text('Tipo de imagen no permitido')
			return false
		}
		else{
			$('#txA').css(normal).text('')
			var formu = new FormData($('#sl_ntB')[0])
			$.ajax({
				url: '../../../mofimgnotic.php',
				type: 'POST',
				data: formu,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#txA").prepend(loading)
				},
				success:reulimg,
				error:function () {
					$("#txA").css(mal).text("Ocurrió un error")
					$("#txA").fadeIn();$("#txA").fadeOut(3000)
				}
			})
			return false
		}
	}
}

function mofdat_noticia () {
	var ida = $(this).attr('data-id')
	var fa=$('#ttnt').val()
	var fb=$('#urnt').val()
	var fc=$('#xxnt').val()
	if (fa === '') {
		$('#txB').css(mal).text('Ingrese el título')
		return false
	}
	else{
		$('#txB').css(normal).text('')
			.prepend(loading)
		$.post('mofdatnotic.php', {id:ida, a:fa, b:fb, c:fc}, resuldat)
		return false
	}
}

function resuldat (res) {
	if (res == '2') {
		$('#txB').css(bien).text('Datos modificado')
		location.reload(20)
	}
	else{
		$('#txB').css(mal).html(res)
		return false
	}
}
