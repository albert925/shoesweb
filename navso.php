<?php
 /*
 	Función para dtectar el sistema operativo, navegador y version
 */
 	function detect()
 	{
 		$browser = ['IE','Opera','Mozilla','Netscape','Firefox','Safari','Chrome'];
 		$os = ['Win','Mac','Linux'];
 		//definimos unos valores por defecto para el navegador y el sistema operativo
 		$info['browser'] = "OTHER";
 		$info['os'] = "OTHER";

 		# buscamos el navegador con su sistema operativo
 		foreach($browser as $parent)
 		{
 			$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
			$f = $s + strlen($parent);
			$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 15);
			$version = preg_replace('/[^0-9,.]/','',$version);
			if ($s){
				$info['browser'] = $parent;
				$info['version'] = $version;
			}
		}
		# obtenemos el sistema operativo
		foreach($os as $val)
		{
			if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']),$val)!==false)
			$info['os'] = $val;
		}
		# devolvemos el array de valores
		return $info;
 	}

 	$info = detect();

	$so = $info['os'];
	$nv = $info['browser'];
	$vr = $info['version'];
?>