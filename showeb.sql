-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-03-2016 a las 21:00:50
-- Versión del servidor: 10.1.8-MariaDB
-- Versión de PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `showeb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id_adm` int(11) NOT NULL,
  `user_adm` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `pass_adm` varchar(800) COLLATE utf8_spanish_ci NOT NULL,
  `tp_adm` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `es_adm` varchar(10) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_adm`, `user_adm`, `pass_adm`, `tp_adm`, `es_adm`) VALUES
(1, 'albert', '6d0b3a4584147538235ee523609fa3333bcdd3f4', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id_cd` int(11) NOT NULL,
  `pais_id` int(11) DEFAULT NULL,
  `dpt_id` int(11) DEFAULT NULL,
  `nam_cd` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientep`
--

CREATE TABLE `clientep` (
  `id_cl` int(11) NOT NULL,
  `tt_cl` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clientep`
--

INSERT INTO `clientep` (`id_cl`, `tt_cl`) VALUES
(2, 'cliente1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `id_dpt` int(11) NOT NULL,
  `pais_ip` int(11) NOT NULL,
  `nam_dpt` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones_us`
--

CREATE TABLE `direcciones_us` (
  `id_dr_us` int(11) NOT NULL,
  `us_id` int(11) NOT NULL,
  `recibe` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `barrio` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `tel_dr` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `ip_os_dr` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id_emp` int(11) NOT NULL,
  `us_id` int(11) NOT NULL,
  `nm_emp` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `nit_emp` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `tel_emp` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `cel_emp` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `pais_emp` int(11) DEFAULT NULL,
  `dep_emp` int(11) DEFAULT NULL,
  `cd_emp` int(11) DEFAULT NULL,
  `dire_emp` text COLLATE utf8_spanish_ci NOT NULL,
  `barr_emp` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fec_emp` date NOT NULL,
  `hor_emp` time NOT NULL,
  `ip_emp` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `es_emp` varchar(10) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `id_f` int(3) UNSIGNED ZEROFILL NOT NULL,
  `fe_f` date NOT NULL,
  `hh_f` time NOT NULL,
  `nv_f` decimal(10,0) NOT NULL,
  `p_id` int(11) NOT NULL,
  `us_id` int(11) NOT NULL,
  `vu_f` decimal(10,0) NOT NULL,
  `ct_f` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `sb_f` decimal(10,0) NOT NULL,
  `tt_f` decimal(10,0) NOT NULL,
  `es_f` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `ipso_f` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcasp`
--

CREATE TABLE `marcasp` (
  `id_mk` int(11) NOT NULL,
  `tt_mk` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `rt_mk` varchar(550) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id_pais` int(11) NOT NULL,
  `nam_pais` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id_p` int(11) NOT NULL,
  `cl_id` int(11) NOT NULL,
  `tp_id` int(11) DEFAULT NULL,
  `mk_id` int(11) DEFAULT NULL,
  `tt_p` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `pca_p` decimal(10,0) NOT NULL,
  `pc_p` decimal(10,0) NOT NULL,
  `xx_p` text COLLATE utf8_spanish_ci NOT NULL,
  `es_p` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `quien_p` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `ip_p` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `nv_p` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `so_p` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `pais_p` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `usi_p` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_images`
--

CREATE TABLE `producto_images` (
  `id_img_p` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `rt_p` varchar(455) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesiones_adm`
--

CREATE TABLE `sesiones_adm` (
  `id_sesadm` int(11) NOT NULL,
  `adm_id` int(11) NOT NULL,
  `ip_sesadm` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `nv_sesadm` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `so_sesadm` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `lg_sesadm` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fei_sesadm` datetime NOT NULL,
  `fef_sesadm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `sesiones_adm`
--

INSERT INTO `sesiones_adm` (`id_sesadm`, `adm_id`, `ip_sesadm`, `nv_sesadm`, `so_sesadm`, `lg_sesadm`, `fei_sesadm`, `fef_sesadm`) VALUES
(1, 1, '127.0.0.1', 'Firefox', 'Win', 'Colombia', '2016-03-16 00:00:00', '2016-03-16 00:00:00'),
(2, 1, '127.0.0.1', 'Firefox', 'Win', 'Colombia', '2016-03-16 00:00:00', '2016-03-16 21:25:57'),
(3, 1, '127.0.0.1', 'Firefox', 'Win', 'Colombia', '2016-03-16 21:26:23', '2016-03-16 21:44:01'),
(4, 1, '127.0.0.1', 'Firefox', 'Win', 'Colombia', '2016-03-16 21:44:26', '2016-03-16 23:17:06'),
(5, 1, '127.0.0.1', 'Firefox', 'Win', 'Colombia', '2016-03-16 23:17:22', '2016-03-16 23:17:34'),
(6, 1, '127.0.0.1', 'Firefox', 'Win', 'Colombia', '2016-03-16 23:17:43', '0000-00-00 00:00:00'),
(7, 1, '127.0.0.1', 'Firefox', 'Win', 'Colombia', '2016-03-17 18:13:03', '0000-00-00 00:00:00'),
(8, 1, '127.0.0.1', 'Firefox', 'Win', 'Colombia', '2016-03-17 18:38:46', '2016-03-17 18:38:50'),
(9, 1, '127.0.0.1', 'Firefox', 'Win', 'Colombia', '2016-03-17 18:39:17', '2016-03-17 18:39:21'),
(10, 1, '127.0.0.1', 'Firefox', 'Win', 'Colombia', '2016-03-17 18:39:35', '0000-00-00 00:00:00'),
(11, 1, '127.0.0.1', 'Firefox', 'Win', 'Colombia', '2016-03-18 20:03:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesiones_us`
--

CREATE TABLE `sesiones_us` (
  `id_ss` int(11) NOT NULL,
  `us_id` int(11) NOT NULL,
  `ip_ss` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `nv_ss` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `so_ss` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `lg_ss` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fei_ss` date NOT NULL,
  `fef_ss` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slid_not`
--

CREATE TABLE `slid_not` (
  `id_snt` int(11) NOT NULL,
  `tt_snt` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `rt_snt` varchar(455) COLLATE utf8_spanish_ci NOT NULL,
  `xx_snt` text COLLATE utf8_spanish_ci NOT NULL,
  `lk_snt` varchar(550) COLLATE utf8_spanish_ci NOT NULL,
  `es_snt` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fe_snt` date NOT NULL,
  `ip_snt` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `slid_not`
--

INSERT INTO `slid_not` (`id_snt`, `tt_snt`, `rt_snt`, `xx_snt`, `lk_snt`, `es_snt`, `fe_snt`, `ip_snt`) VALUES
(1, 'Creemos gran estilo se logra a trav&eacute;s de relaciones con personas reales.', 'images/noticias/33.jpg', 'Es por eso que hacer coincidir con un estilista, como en una persona cuyo verdadero singular mision es determinar el mejor estilo al conocer el verdadero.', '', '1', '2016-03-17', '127.0.0.1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tallasp`
--

CREATE TABLE `tallasp` (
  `id_tll` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `nm_tll` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `preca_tll` decimal(10,0) NOT NULL,
  `prec_tll` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipop`
--

CREATE TABLE `tipop` (
  `id_tp` int(11) NOT NULL,
  `cl_id` int(11) DEFAULT NULL,
  `tt_tp` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_us` int(11) NOT NULL,
  `id_face` varchar(400) COLLATE utf8_spanish_ci NOT NULL,
  `name_users` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `us_us` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `nam_us` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `ap_us` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `cor_us` varchar(455) COLLATE utf8_spanish_ci NOT NULL,
  `pass_us` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `cred_us` decimal(10,0) NOT NULL,
  `avt_us` varchar(455) COLLATE utf8_spanish_ci NOT NULL,
  `pais_us` int(11) DEFAULT NULL,
  `dep_us` int(11) DEFAULT NULL,
  `cd_us` int(11) DEFAULT NULL,
  `cc_us` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `tel_us` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `mov_us` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `dir_us` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `barr_us` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `tp_us` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `es_us` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `cod_us` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `corrcb_us` varchar(455) COLLATE utf8_spanish_ci NOT NULL,
  `fec_us` date NOT NULL,
  `ip_os_lg` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id_adm`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id_cd`),
  ADD KEY `pais` (`pais_id`),
  ADD KEY `departamento` (`dpt_id`);

--
-- Indices de la tabla `clientep`
--
ALTER TABLE `clientep`
  ADD PRIMARY KEY (`id_cl`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id_dpt`),
  ADD KEY `pais` (`pais_ip`);

--
-- Indices de la tabla `direcciones_us`
--
ALTER TABLE `direcciones_us`
  ADD PRIMARY KEY (`id_dr_us`),
  ADD KEY `usuario` (`us_id`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id_emp`),
  ADD KEY `pais` (`pais_emp`),
  ADD KEY `departamento` (`dep_emp`),
  ADD KEY `ciudad_o_munic` (`cd_emp`),
  ADD KEY `usuarioPagina` (`us_id`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id_f`),
  ADD KEY `producto` (`p_id`),
  ADD KEY `usuario` (`us_id`);

--
-- Indices de la tabla `marcasp`
--
ALTER TABLE `marcasp`
  ADD PRIMARY KEY (`id_mk`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_p`),
  ADD KEY `cliente` (`cl_id`),
  ADD KEY `tipos` (`tp_id`),
  ADD KEY `marcas` (`mk_id`),
  ADD KEY `ususario_ingresopp` (`usi_p`);

--
-- Indices de la tabla `producto_images`
--
ALTER TABLE `producto_images`
  ADD KEY `producto` (`p_id`);

--
-- Indices de la tabla `sesiones_adm`
--
ALTER TABLE `sesiones_adm`
  ADD PRIMARY KEY (`id_sesadm`),
  ADD KEY `admin` (`adm_id`);

--
-- Indices de la tabla `sesiones_us`
--
ALTER TABLE `sesiones_us`
  ADD PRIMARY KEY (`id_ss`),
  ADD KEY `usuario` (`us_id`);

--
-- Indices de la tabla `slid_not`
--
ALTER TABLE `slid_not`
  ADD PRIMARY KEY (`id_snt`);

--
-- Indices de la tabla `tallasp`
--
ALTER TABLE `tallasp`
  ADD PRIMARY KEY (`id_tll`),
  ADD KEY `producto` (`p_id`);

--
-- Indices de la tabla `tipop`
--
ALTER TABLE `tipop`
  ADD PRIMARY KEY (`id_tp`),
  ADD KEY `cliente` (`cl_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_us`),
  ADD KEY `paisus` (`pais_us`),
  ADD KEY `departus` (`dep_us`),
  ADD KEY `ciudadus` (`cd_us`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id_adm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `id_cd` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `clientep`
--
ALTER TABLE `clientep`
  MODIFY `id_cl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `id_dpt` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `direcciones_us`
--
ALTER TABLE `direcciones_us`
  MODIFY `id_dr_us` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id_emp` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `id_f` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `marcasp`
--
ALTER TABLE `marcasp`
  MODIFY `id_mk` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id_p` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sesiones_adm`
--
ALTER TABLE `sesiones_adm`
  MODIFY `id_sesadm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `sesiones_us`
--
ALTER TABLE `sesiones_us`
  MODIFY `id_ss` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `slid_not`
--
ALTER TABLE `slid_not`
  MODIFY `id_snt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tallasp`
--
ALTER TABLE `tallasp`
  MODIFY `id_tll` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipop`
--
ALTER TABLE `tipop`
  MODIFY `id_tp` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_us` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `ciudad_ibfk_1` FOREIGN KEY (`pais_id`) REFERENCES `pais` (`id_pais`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ciudad_ibfk_2` FOREIGN KEY (`dpt_id`) REFERENCES `departamento` (`id_dpt`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD CONSTRAINT `departamento_ibfk_1` FOREIGN KEY (`pais_ip`) REFERENCES `pais` (`id_pais`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `direcciones_us`
--
ALTER TABLE `direcciones_us`
  ADD CONSTRAINT `direcciones_us_ibfk_1` FOREIGN KEY (`us_id`) REFERENCES `usuarios` (`id_us`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD CONSTRAINT `empresa_ibfk_1` FOREIGN KEY (`us_id`) REFERENCES `usuarios` (`id_us`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `empresa_ibfk_2` FOREIGN KEY (`pais_emp`) REFERENCES `pais` (`id_pais`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `empresa_ibfk_3` FOREIGN KEY (`dep_emp`) REFERENCES `departamento` (`id_dpt`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `empresa_ibfk_4` FOREIGN KEY (`cd_emp`) REFERENCES `ciudad` (`id_cd`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `producto` (`id_p`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `factura_ibfk_2` FOREIGN KEY (`us_id`) REFERENCES `usuarios` (`id_us`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`cl_id`) REFERENCES `clientep` (`id_cl`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_ibfk_2` FOREIGN KEY (`tp_id`) REFERENCES `tipop` (`id_tp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_ibfk_3` FOREIGN KEY (`mk_id`) REFERENCES `marcasp` (`id_mk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto_images`
--
ALTER TABLE `producto_images`
  ADD CONSTRAINT `producto_images_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `producto` (`id_p`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sesiones_adm`
--
ALTER TABLE `sesiones_adm`
  ADD CONSTRAINT `sesiones_adm_ibfk_1` FOREIGN KEY (`adm_id`) REFERENCES `administrador` (`id_adm`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sesiones_us`
--
ALTER TABLE `sesiones_us`
  ADD CONSTRAINT `sesiones_us_ibfk_1` FOREIGN KEY (`us_id`) REFERENCES `usuarios` (`id_us`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tallasp`
--
ALTER TABLE `tallasp`
  ADD CONSTRAINT `tallasp_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `producto` (`id_p`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tipop`
--
ALTER TABLE `tipop`
  ADD CONSTRAINT `tipop_ibfk_1` FOREIGN KEY (`cl_id`) REFERENCES `clientep` (`id_cl`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`pais_us`) REFERENCES `pais` (`id_pais`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`dep_us`) REFERENCES `departamento` (`id_dpt`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuarios_ibfk_3` FOREIGN KEY (`cd_us`) REFERENCES `ciudad` (`id_cd`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
