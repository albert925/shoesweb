<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<meta name="description" content="zapaos" />
	<meta name="keywords" content="zapatos" />
	<title>Zapatos</title>
	<link rel="stylesheet" href="css/normalize.css" />
	<link rel="stylesheet" href="css/iconos/style.css" />
	<link rel="stylesheet" href="css/owl_carousel.css" />
	<link rel="stylesheet" href="css/owl_theme_min.css" />
	<link rel="stylesheet" href="css/loadingw8.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/inicio.css" />
	<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="js/scrippag.js"></script>
	<script src="js/owl_carousel_min.js"></script>
</head>
<body>
	<section class="container">
		<nav id="mnV" class="mnnv">
			<a href="">Inicio</a>
			<a href="contacto">Contacto</a>
		</nav>
		<header class="hea">
			<figure>
				<img src="images/head1.jpg" alt="header1" />
			</figure>
			<h1>Diseña <br /> Tus prendas <br /> A tu medida</h1>
		</header>
		<section>
			<div class="hoz mn1">
				<h2>Hozier Club</h2>
				<div class="busqgoo"></div>
			</div>
			<nav id="mnP">
				<nav>
					<a href="">Inicio</a>
					<a href="contacto">Contacto</a>
				</nav>
				<div id="btn_menu">
					<span class="icon-menu"></span>
				</div>
				<div id="carus">
					<div id="carrito">
						<span class="icon-carritoi"></span>
					</div>
					<div id="avlt">
						<figure></figure>
						<div>Entrar</div>
					</div>
				</div>
			</nav>
			<section class="sg3">
				<article class="margen ss1">
					<figure class="de" data-tp="1">
						<img src="images/fig1.jpg" alt="imagen" />
						<figcaption>
							<h2>Camisa de Hombre</h2>
						</figcaption>
					</figure>
					<figure class="iz" data-tp="2">
						<img src="images/fig1.jpg" alt="imagen" />
						<figcaption>
							<h2>Camisa de Hombre</h2>
						</figcaption>
					</figure>
					<figure class="de" data-tp="3">
						<img src="images/fig1.jpg" alt="imagen" />
						<figcaption>
							<h2>Camisa de Hombre</h2>
						</figcaption>
					</figure>
				</article>
			</section>
			<section class="ss2 sg1">
				<article class="margen">
					<article class="owl-carousel owl-theme owl-loaded">
						<div class="item">
							<article class="flnt">
								<figcaption>
									<h2>Creemos gran estilo se logra a través de relaciones con personas reales.</h2>
									<p>
										Es por eso que hacer coincidir con un estilista, como en una persona cuyo 
										verdadero singular mision es determinar el mejor estilo al conocer el verdadero.
									</p>
									<div class="flabtn">
										<a class="cl1" href="">Comprar</a>
										<a class="cl2" href="">Ver más..</a>
									</div>
								</figcaption>
								<figure>
									<img src="images/noticias/3364599-3-MULTIVIEW.jpg" alt="zapato" />
								</figure>
							</article>
						</div>
					</article>
				</article>
			</section>
			<section class="ss3 sg1">
				<figure class="margen">
					<div>
						<img src="images/sec2.jpg" alt="sec2" />
					</div>
					<figcaption>
						<h2>Devoluciones libre y fácil, siempre</h2>
						<p>
							Estamos 100% detrás de nuestros productos. 
							Si no estás satisfecho con su compra por cualquier razón, le daremos 
							la opción de un reembolso completo o intercambio del producto
						</p>
					</figcaption>
				</figure>
			</section>
			<footer>
				<article class="flxfoot margen">
					<article id="ar1" class="colmg">
						<h2 id="hGG">Calzado</h2>
					</article>
					<article class="colmg">
						<h2>Contacto</h2>
						<div><b>Cúcuta, Colombia</b></div>
						<div>hello@miweb.com</div>
						<div>+57 314 5678965</div>
						<div class="redes">
							<a href="" target="_blank"><span class="icon-facebook5"></span></a>
							<a href="" target="_blank"><span class="icon-twitter"></span></a>
							<a href="" target="_blank"><span class="icon-instagram"></span></a>
						</div>
					</article>
					<article class="colmg">
						<h2>Comprar Seguro</h2>
						<div class="flpag">
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
						</div>
						<div id="termcod">
							<b><a href="terminos">Términos</a> y <a href="condiciones">condiciones</a></b>
						</div>
					</article>
				</article>
			</footer>
		</section>
	</section>
	<script src="js/sliddiv.js"></script>
</body>
</html>