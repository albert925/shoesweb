<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<meta name="description" content="Ingreso adm" />
	<title>Ingreso administrador |Zapatos</title>
	<link rel="stylesheet" href="../css/normalize.css" />
	<link rel="stylesheet" href="../css/iconos/style.css" />
	<link rel="stylesheet" href="../css/loadingw8.css" />
	<link rel="stylesheet" href="../css/style.css" />
	<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="../js/scrippag.js"></script>
	<script src="../js/lioad.js"></script>
</head>
<body>
	<section class="container">
		<nav id="mnV" class="mnnv">
			<a href="../">Inicio</a>
			<a href="../contacto">Contacto</a>
		</nav>
		<header class="hea">
			<figure>
				<img src="../images/head1.jpg" alt="header1" />
			</figure>
			<h1>Ingreso Administrador</h1>
		</header>
		<section>
			<div class="hoz mn1">
				<h2>Hozier Club</h2>
				<div class="busqgoo"></div>
			</div>
			<nav id="mnP">
				<nav>
					<a href="../">Inicio</a>
					<a href="../contacto">Contacto</a>
				</nav>
				<div id="btn_menu">
					<span class="icon-menu"></span>
				</div>
				<div id="carus">
					<div id="carrito">
						<span class="icon-carritoi"></span>
					</div>
					<div id="avlt">
						<figure></figure>
						<div>Entrar</div>
					</div>
				</div>
			</nav>
			<section class="sg4">
				<article class="margen">
					<form action="#" method="post" class="columninput ssdv">
						<label><b>Usuario</b></label>
						<input type="text" id="usad" name="usad" required />
						<label><b>Contraseña</b></label>
						<input type="password" id="psad" name="psad" required />
						<div id="txA"></div>
						<div class="nbsub">
							<input type="submit" value="Ingresar" id="biad" />
						</div>
					</form>
				</article>
			</section>
			<footer>
				<article class="flxfoot margen">
					<article id="ar1" class="colmg">
						<h2 id="hGG">Calzado</h2>
					</article>
					<article class="colmg">
						<h2>Contacto</h2>
						<div><b>Cúcuta, Colombia</b></div>
						<div>hello@miweb.com</div>
						<div>+57 314 5678965</div>
						<div class="redes">
							<a href="" target="_blank"><span class="icon-facebook5"></span></a>
							<a href="" target="_blank"><span class="icon-twitter"></span></a>
							<a href="" target="_blank"><span class="icon-instagram"></span></a>
						</div>
					</article>
					<article class="colmg">
						<h2>Comprar Seguro</h2>
						<div class="flpag">
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
						</div>
						<div id="termcod">
							<b><a href="terminos">Términos</a> y <a href="condiciones">condiciones</a></b>
						</div>
					</article>
				</article>
			</footer>
		</section>
	</section>
</body>
</html>