<?php
	include '../../../config.php';
	session_start();
	if (isset($_SESSION['adm'])) {
		$iadR = $_SESSION['adm'];
		$sqadm = "SELECT * from administrador where id_adm=$iadR";
		$readm = $conexion->query($sqadm) or die ('Error sql');
		while ($ad = $readm->fetch_assoc()) {
			$idad = $ad['id_adm'];
			$usad = $ad['user_adm'];
			$tpad = $ad['tp_adm'];
			$esad = $ad['es_adm'];
		}
		//num_rows
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<meta name="description" content="Ingreso adm" />
	<title>Noticias |Zapatos</title>
	<link rel="stylesheet" href="../../../css/normalize.css" />
	<link rel="stylesheet" href="../../../css/iconos/style.css" />
	<link rel="stylesheet" href="../../../css/loadingw8.css" />
	<link rel="stylesheet" href="../../../css/style.css" />
	<link rel="stylesheet" href="../../../css/styadm.css" />
	<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="../../../js/scrippag.js"></script>
	<script src="../../../js/scripadm.js"></script>
</head>
<body>
	<section class="container">
		<nav id="mnV" class="mnnv">
			<a href="../">Inicio</a>
			<a href="../noticias">Noticias</a>
			<a href="../productos">Productos</a>
			<a href="../usuarios">Usuarios</a>
			<a href="../">Contabilidad</a>
		</nav>
		<header class="hea">
			<figure>
				<img src="../../../images/head1.jpg" alt="header1" />
			</figure>
		</header>
		<section>
			<div class="hoz mn1">
				<h2>Noticias</h2>
			</div>
			<nav id="mnP">
				<nav>
					<a href="../">Inicio</a>
					<a href="../noticias">Noticias</a>
					<a href="../productos">Productos</a>
					<a href="../usuarios">Usuarios</a>
					<a href="../">Contabilidad</a>
				</nav>
				<div id="btn_menu">
					<span class="icon-menu"></span>
				</div>
				<div id="carus">
					<div id="admus">
						<a href="../"><?php echo "$usad"; ?></a>
						<a href="../../../cerrar">Salir</a>
					</div>
				</div>
			</nav>
			<nav id="mnS">
				<a id="bcd" href="">Nueva Noticia</a>
			</nav>
			<article class="margen caja">
				<form action="#" method="post" enctype="multipart/form-data" class="columninput" id="sl_nt">
					<label>*<b>Título</b></label>
					<input type="text" id="ttnt" name="ttnt" required />
					<label>*<b>Imagen (resolución 480 x 360)</b></label>
					<input type="file" id="ignt" name="ignt" required />
					<label><b>Link</b></label>
					<input type="url" id="urnt" name="urnt" />
					<label><b>Texto</b></label>
					<textarea id="xxnt" name="xxnt" rows="3"></textarea>
					<div id='txA'></div>
					<input type="submit" value="Ingresar" id="nvnt" />
				</form>
			</article>
			<section class="sg4">
				<h1>Noticias</h1>
				<article class="margen sg2 flxB">
					<?php
						error_reporting(E_ALL ^ E_NOTICE);
						$tamno_pagina=15;
						$pagina= $_GET['pagina'];
						if (!$pagina) {
							$inicio=0;
							$pagina=1;
						}
						else{
							$inicio= ($pagina - 1)*$tamno_pagina;
						}
						$ssql="SELECT * from slid_not order by id_snt desc";
						$rs=$conexion->query($ssql) or die (mysqli_error());
						$num_total_registros= $rs->num_rows;
						$total_paginas= ceil($num_total_registros / $tamno_pagina);
						$gsql="SELECT * from slid_not order by id_snt desc limit $inicio, $tamno_pagina";
						$impsql=$conexion->query($gsql) or die (mysqli_error());
						while ($gh=$impsql->fetch_assoc()) {
							$idN = $gh['id_snt'];
							$ttN = $gh['tt_snt'];
							$ltN = $gh['lk_snt'];
							$xxN = $gh['xx_snt'];
							$trN = $gh['rt_snt'];
							$esN = $gh['es_snt'];
					?>
					<figure id="cjac<?php echo $idN ?>" class="fcaj">
						<h2><?php echo "$ttN"; ?></h2>
						<img src="../../../<?php echo $trN ?>" alt="<?php echo $ttN ?>" />
						<figcaption class="columninput columcent">
							<a id="dslmf" href="mof_noti.php?nt=<?php echo $idN ?>">Modificar</a>
							<a class="doll" href="borr_noticia.php" data-id="<?php echo $idN ?>">Borrar</a>
						</figcaption>
					</figure>
					<?php
						}
					?>
				</article>
				<article class="margen sg2 ">
					<br />
					<b>Páginas: </b>
					<?php
						//muestro los distintos indices de las paginas
						if ($total_paginas>1) {
							for ($i=1; $i <=$total_paginas ; $i++) { 
								if ($pagina==$i) {
									//si muestro el indice del la pagina actual, no coloco enlace
						?>
							<b><?php echo $pagina." "; ?></b>
						<?php
								}
								else{
									//si el índice no corresponde con la página mostrada actualmente, coloco el enlace para ir a esa página 
						?>
									<a href="index.php?pagina=<?php echo $i ?>"><?php echo "$i"; ?></a>

						<?php
								}
							}
						}
					?>
				</article>
			</section>
			<footer>
				<article class="flxfoot margen">
					<article id="ar1" class="colmg">
						<h2 id="hGG">Calzado</h2>
					</article>
					<article class="colmg">
						<h2>Contacto</h2>
						<div><b>Cúcuta, Colombia</b></div>
						<div>hello@miweb.com</div>
						<div>+57 314 5678965</div>
						<div class="redes">
							<a href="" target="_blank"><span class="icon-facebook5"></span></a>
							<a href="" target="_blank"><span class="icon-twitter"></span></a>
							<a href="" target="_blank"><span class="icon-instagram"></span></a>
						</div>
					</article>
					<article class="colmg">
						<h2>Comprar Seguro</h2>
						<div class="flpag">
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
						</div>
						<div id="termcod">
							<b><a href="terminos">Términos</a> y <a href="condiciones">condiciones</a></b>
						</div>
					</article>
				</article>
			</footer>
		</section>
	</section>
	<script src="../../../js/notadm.js"></script>
</body>
</html>
<?php
	}
	else{
?>
<script type="text/javascript">
	window.location.href="../../erroradm.html"
</script>
<?php
	}
?>