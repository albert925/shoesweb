<?php
	include '../../../config.php';
	session_start();
	if (isset($_SESSION['adm'])) {
		$iadR = $_SESSION['adm'];
		$sqadm = "SELECT * from administrador where id_adm=$iadR";
		$readm = $conexion->query($sqadm) or die ('Error sql');
		while ($ad = $readm->fetch_assoc()) {
			$idad = $ad['id_adm'];
			$usad = $ad['user_adm'];
			$tpad = $ad['tp_adm'];
			$esad = $ad['es_adm'];
		}
		//num_rows
		$idR = $_GET['nt'];
		if ($idR == "") {
?>
<script type="text/javascript">
	alert('Id de habitacion no disponible')
	window.location.href = '../noticias'
</script>
<?php
		}
		else{
			$datos = "SELECT * from slid_not where id_snt=$idR";
			$sql_datos = $conexion->query($datos) or die ('Error 1');
			$num_datos = $sql_datos->num_rows;
			if ($num_datos > 0) {
				while ($dt = $sql_datos->fetch_assoc()) {
					$idN = $dt['id_snt'];
					$ttN = $dt['tt_snt'];
					$ltN = $dt['lk_snt'];
					$xxN = $dt['xx_snt'];
					$trN = $dt['rt_snt'];
					$esN = $dt['es_snt'];
				}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<meta name="description" content="Ingreso adm" />
	<title><?php echo "$ttN"; ?> |Zapatos</title>
	<link rel="stylesheet" href="../../../css/normalize.css" />
	<link rel="stylesheet" href="../../../css/iconos/style.css" />
	<link rel="stylesheet" href="../../../css/loadingw8.css" />
	<link rel="stylesheet" href="../../../css/style.css" />
	<link rel="stylesheet" href="../../../css/styadm.css" />
	<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="../../../js/scrippag.js"></script>
	<script src="../../../js/scripadm.js"></script>
</head>
<body>
	<section class="container">
		<nav id="mnV" class="mnnv">
			<a href="../">Inicio</a>
			<a href="../noticias">Noticias</a>
			<a href="../productos">Productos</a>
			<a href="../usuarios">Usuarios</a>
			<a href="../">Contabilidad</a>
		</nav>
		<header class="hea">
			<figure>
				<img src="../../../images/head1.jpg" alt="header1" />
			</figure>
		</header>
		<section>
			<div class="hoz mn1">
				<h2><?php echo "$ttN"; ?></h2>
			</div>
			<nav id="mnP">
				<nav>
					<a href="../">Inicio</a>
					<a href="../noticias">Noticias</a>
					<a href="../productos">Productos</a>
					<a href="../usuarios">Usuarios</a>
					<a href="../">Contabilidad</a>
				</nav>
				<div id="btn_menu">
					<span class="icon-menu"></span>
				</div>
				<div id="carus">
					<div id="admus">
						<a href="../"><?php echo "$usad"; ?></a>
						<a href="../../../cerrar">Salir</a>
					</div>
				</div>
			</nav>
			<nav id="mnS">
				<a href="../noticias">Ver Noticia</a>
			</nav>
			<section class="sg4">
				<h1><?php echo "$ttN"; ?></h1>
				<article class="margen">
					<form action="#" method="post" enctype="multipart/form-data" class="columninput ssdv" id="sl_ntB">
						<h2>Imagen</h2>
						<a href="../../../<?php echo $trN ?>" target="_blank"><?php echo $trN ?></a>
						<input type="text" id="idc" name="idc" value="<?php echo $idR ?>" required style="display: none;" />
						<label>*<b>Imagen (resolución 480 x 360)</b></label>
						<input type="file" id="ignt" name="ignt" required />
						<div id='txA'></div>
						<input type="submit" value="Cambiar" id="mfig" data-id="<?php echo $idR ?>" />
					</form>
					<form action="#" method="post" class="columninput ssdv">
						<h2>Datos</h2>
						<label>*<b>Título</b></label>
						<input type="text" id="ttnt" name="ttnt" value="<?php echo $ttN ?>" required />
						<label><b>Link</b></label>
						<input type="url" id="urnt" name="urnt" value="<?php echo $ltN ?>" />
						<label><b>Texto</b></label>
						<textarea id="xxnt" name="xxnt" rows="3"><?php echo "$xxN"; ?></textarea>
						<div id='txB'></div>
						<input type="submit" value="Modificar" id="mfnt" data-id="<?php echo $idR ?>" />
					</form>
				</article>
			</section>
			<footer>
				<article class="flxfoot margen">
					<article id="ar1" class="colmg">
						<h2 id="hGG">Calzado</h2>
					</article>
					<article class="colmg">
						<h2>Contacto</h2>
						<div><b>Cúcuta, Colombia</b></div>
						<div>hello@miweb.com</div>
						<div>+57 314 5678965</div>
						<div class="redes">
							<a href="" target="_blank"><span class="icon-facebook5"></span></a>
							<a href="" target="_blank"><span class="icon-twitter"></span></a>
							<a href="" target="_blank"><span class="icon-instagram"></span></a>
						</div>
					</article>
					<article class="colmg">
						<h2>Comprar Seguro</h2>
						<div class="flpag">
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
						</div>
						<div id="termcod">
							<b><a href="terminos">Términos</a> y <a href="condiciones">condiciones</a></b>
						</div>
					</article>
				</article>
			</footer>
		</section>
	</section>
	<script src="../../../js/notadm.js"></script>
</body>
</html>
<?php
			}
			else{
?>
<script type="text/javascript">
	alert('Noticia no existe o ha sido eliminada')
	window.location.href="../noticias"
</script>
<?php
			}
		}
	}
	else{
?>
<script type="text/javascript">
	window.location.href="../../erroradm.html"
</script>
<?php
	}
?>