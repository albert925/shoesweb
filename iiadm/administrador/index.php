<?php
	include '../../config.php';
	session_start();
	if (isset($_SESSION['adm'])) {
		$iadR = $_SESSION['adm'];
		$sqadm = "SELECT * from administrador where id_adm=$iadR";
		$readm = $conexion->query($sqadm) or die ('Error sql');
		while ($ad = $readm->fetch_assoc()) {
			$idad = $ad['id_adm'];
			$usad = $ad['user_adm'];
			$tpad = $ad['tp_adm'];
			$esad = $ad['es_adm'];
		}
		//num_rows
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<meta name="description" content="Ingreso adm" />
	<title>administradro <?php echo "$usad"; ?> |Zapatos</title>
	<link rel="stylesheet" href="../../css/normalize.css" />
	<link rel="stylesheet" href="../../css/iconos/style.css" />
	<link rel="stylesheet" href="../../css/loadingw8.css" />
	<link rel="stylesheet" href="../../css/style.css" />
	<link rel="stylesheet" href="../../css/styadm.css" />
	<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="../../js/scrippag.js"></script>
	<script src="../../js/lioad.js"></script>
</head>
<body>
	<section class="container">
		<nav id="mnV" class="mnnv">
			<a href="">Inicio</a>
			<a href="noticias">Noticias</a>
			<a href="productos">Productos</a>
			<a href="usuarios">Usuarios</a>
			<a href="">Contabilidad</a>
		</nav>
		<header class="hea">
			<figure>
				<img src="../../images/head1.jpg" alt="header1" />
			</figure>
		</header>
		<section>
			<div class="hoz mn1">
				<h2>Hozier Club</h2>
			</div>
			<nav id="mnP">
				<nav>
					<a href="">Inicio</a>
					<a href="noticias">Noticias</a>
					<a href="productos">Productos</a>
					<a href="usuarios">Usuarios</a>
					<a href="">Contabilidad</a>
				</nav>
				<div id="btn_menu">
					<span class="icon-menu"></span>
				</div>
				<div id="carus">
					<div id="admus">
						<a href=""><?php echo "$usad"; ?></a>
						<a href="../../cerrar">Salir</a>
					</div>
				</div>
			</nav>
			<section class="sg4">
				<article class="margen">
					<h1>Datos</h1>
					<article class="flxA">
						<article class="columninput ssdv">
							<h2>Usuaro</h2>
							<input type="text" id="fusad" value="<?php echo $usad ?>" required />
							<div id="txA"></div>
							<input type="submit" value="Cambiar" id="camA" data-id="<?php echo $idad ?>" />
						</article>
						<article class="columninput ssdv">
							<h2>Contraseña</h2>
							<label><b>Contraseña actual</b></label>
							<input type="password" id="cac" required />
							<label><b>Contraseña nueva</b></label>
							<input type="password" id="nna" required />
							<label><b>Repite la contraseña nueva</b></label>
							<input type="password" id="nnb" required />
							<div id="txB"></div>
							<input type="submit" value="Cambiar" id="camB" data-id="<?php echo $idad ?>" />
						</article>
					</article>
				</article>
			</section>
			<footer>
				<article class="flxfoot margen">
					<article id="ar1" class="colmg">
						<h2 id="hGG">Calzado</h2>
					</article>
					<article class="colmg">
						<h2>Contacto</h2>
						<div><b>Cúcuta, Colombia</b></div>
						<div>hello@miweb.com</div>
						<div>+57 314 5678965</div>
						<div class="redes">
							<a href="" target="_blank"><span class="icon-facebook5"></span></a>
							<a href="" target="_blank"><span class="icon-twitter"></span></a>
							<a href="" target="_blank"><span class="icon-instagram"></span></a>
						</div>
					</article>
					<article class="colmg">
						<h2>Comprar Seguro</h2>
						<div class="flpag">
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
						</div>
						<div id="termcod">
							<b><a href="terminos">Términos</a> y <a href="condiciones">condiciones</a></b>
						</div>
					</article>
				</article>
			</footer>
		</section>
	</section>
</body>
</html>
<?php
	}
	else{
?>
<script type="text/javascript">
	window.location.href="../erroradm.html"
</script>
<?php
	}
?>