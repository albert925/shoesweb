<?php
	include '../../../config.php';
	session_start();
	if (isset($_SESSION['adm'])) {
		$iadR = $_SESSION['adm'];
		$sqadm = "SELECT * from administrador where id_adm=$iadR";
		$readm = $conexion->query($sqadm) or die ('Error sql');
		while ($ad = $readm->fetch_assoc()) {
			$idad = $ad['id_adm'];
			$usad = $ad['user_adm'];
			$tpad = $ad['tp_adm'];
			$esad = $ad['es_adm'];
		}
		//num_rows
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<meta name="description" content="Ingreso adm" />
	<title>Productos |Zapatos</title>
	<link rel="stylesheet" href="../../../css/normalize.css" />
	<link rel="stylesheet" href="../../../css/iconos/style.css" />
	<link rel="stylesheet" href="../../../css/loadingw8.css" />
	<link rel="stylesheet" href="../../../css/style.css" />
	<link rel="stylesheet" href="../../../css/styadm.css" />
	<link rel="stylesheet" href="../../../css/chosen.css" />
	<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="../../../js/scrippag.js"></script>
	<script src="../../../js/scripadm.js"></script>
	<script src="../../../ckeditor/ckeditor.js"></script>
</head>
<body>
	<section class="container">
		<nav id="mnV" class="mnnv">
			<a href="../">Inicio</a>
			<a href="../noticias">Noticias</a>
			<a href="../productos">Productos</a>
			<a href="../usuarios">Usuarios</a>
			<a href="../">Contabilidad</a>
		</nav>
		<header class="hea">
			<figure>
				<img src="../../../images/head1.jpg" alt="header1" />
			</figure>
		</header>
		<section>
			<div class="hoz mn1">
				<h2>Productos</h2>
			</div>
			<nav id="mnP">
				<nav>
					<a href="../">Inicio</a>
					<a href="../noticias">Noticias</a>
					<a href="../productos">Productos</a>
					<a href="../usuarios">Usuarios</a>
					<a href="../">Contabilidad</a>
				</nav>
				<div id="btn_menu">
					<span class="icon-menu"></span>
				</div>
				<div id="carus">
					<div id="admus">
						<a href="../"><?php echo "$usad"; ?></a>
						<a href="../../../cerrar">Salir</a>
					</div>
				</div>
			</nav>
			<nav id="mnS">
				<a id="bcd" href="">Nuevo Producto</a>
				<a href="cliente.php">Cliente</a>
				<a href="tipos.php">Tipos</a>
				<a href="marcas.php">Marcas</a>
				<a href="imagen_pP.php">Imagen Producto</a>
			</nav>
			<article class="margen caja">
				<form action="new_producto.php" method="post" class="columninput" id="sl_nt">
					<h2>Nuevo producto</h2>
					<label>*<b>Del cliente</b></label>
					<select id="plc" name="plc">
						<option value="0">Selecione</option>
						<?php
							$Acl = "SELECT * from clientep order by id_cl desc";
							$sAcl = $conexion->query($Acl) or die ('Error sql cl');
							while ($lA = $sAcl->fetch_assoc()) {
								$idcl = $lA['id_cl'];
								$ttcl = $lA['tt_cl'];
						?>
						<option value="<?php echo $idcl ?>"><?php echo "$ttcl"; ?></option>
						<?php
							}
						?>
					</select>
					<label>*<b>Del tipo</b></label>
					<select id="ptp" name="ptp">
						<option value="0">Seleccione</option>
						<?php
							$Atp = "SELECT * from tipop order by id_tp desc";
							$sAtp = $conexion->query($Atp) or die ('Error sql tipo');
							while ($tA = $sAtp->fetch_assoc()) {
								$idtp = $tA['id_tp'];
								$tttp = $tA['tt_tp'];
						?>
						<option value="<?php echo $idtp ?>"><?php echo "$tttp"; ?></option>
						<?php
							}
						?>
					</select>
					<label>*<b>De la marca</b></label>
					<select id="mkp" name="mkp">
						<option value="0">Selecione</option>
						<?php
							$Amk = "SELECT * from marcasp order by id_mk desc";
							$sAmk = $conexion->query($Amk) or die ('Error sql marca');
							while ($kA = $sAmk->fetch_assoc()) {
								$idmk = $kA['id_mk'];
								$ttmk = $kA['tt_mk'];
						?>
						<option value="<?php echo $idmk ?>"><?php echo "$ttmk"; ?></option>
						<?php
							}
						?>
					</select>
					<label>*<b>Nombre</b></label>
					<input type="text" id="nmp" name="nmp" required />
					<label><b>Precio anterior</b></label>
					<input type="number" id="ap" name="ap" />
					<label>*<b>Precio general</b></label>
					<input type="number" id="pcp" name="pcp" required />
					<label><b>Descripción</b></label>
					<textarea id="editor1" name="xxp"></textarea>
					<script>
						CKEDITOR.replace('xxp');
					</script>
					<div id="txA"></div>
					<input type="submit" value="Ingresar" id="verP" />
				</form>
			</article>
			<section class="sg4">
				<h1>Productos</h1>
				<article class="margen sg2 flxB">
					<?php
						error_reporting(E_ALL ^ E_NOTICE);
						$tamno_pagina=15;
						$pagina= $_GET['pagina'];
						if (!$pagina) {
							$inicio=0;
							$pagina=1;
						}
						else{
							$inicio= ($pagina - 1)*$tamno_pagina;
						}
						$ssql="SELECT * from producto order by id_p desc";
						$rs=$conexion->query($ssql) or die (mysqli_error());
						$num_total_registros= $rs->num_rows;
						$total_paginas= ceil($num_total_registros / $tamno_pagina);
						$gsql="SELECT * from producto order by id_p desc limit $inicio, $tamno_pagina";
						$impsql=$conexion->query($gsql) or die (mysqli_error());
						while ($gh=$impsql->fetch_assoc()) {
							$idP = $gh['id_p'];
							$clP = $gh['cl_id'];
							$tpP = $gh['tp_id'];
							$mkP = $gh['mk_id'];
							$ttP = $gh['tt_p'];
							$paP = $gh['pca_p'];
							$pcP = $gh['pc_p'];
							$xxP = $gh['xx_p'];
							$esP = $gh['es_p'];
							$quP = $gh['quien_p'];
							$ipP = $gh['ip_p'];
							$nvP = $gh['nv_p'];
							$soP = $gh['so_p'];
							$psP = $gh['pais_p'];
							$usP = $gh['usi_p'];
							$primerImg = "SELECT * from producto_images where p_id=$idP order by id_img_p asc limit 1";
							$sql_primerimg = $conexion->query($primerImg) or die ('#errorimgsql');
							$num_uno = $sql_primerimg->num_rows;
							if ($num_uno > 0) {
								while ($utr = $sql_primerimg->fetch_assoc()) {
									$imgP = $utr['rt_p'];
								}
							}
							else{
								$imgP = "images/predeterminado.png";
							}
					?>
					<figure id="cjac<?php echo $idN ?>" class="fcaj">
						<h2><?php echo "$ttP"; ?></h2>
						<img src="../../../<?php echo $imgP ?>" alt="<?php echo $ttP ?>" />
						<figcaption class="columninput columcent">
							<a id="dslmf" href="mof_producto.php?nt=<?php echo $idN ?>">Modificar</a>
							<a class="doll" href="borr_producto.php" data-id="<?php echo $idP ?>">Borrar</a>
						</figcaption>
					</figure>
					<?php
						}
					?>
				</article>
				<article class="margen sg2 ">
					<br />
					<b>Páginas: </b>
					<?php
						//muestro los distintos indices de las paginas
						if ($total_paginas>1) {
							for ($i=1; $i <=$total_paginas ; $i++) { 
								if ($pagina==$i) {
									//si muestro el indice del la pagina actual, no coloco enlace
						?>
							<b><?php echo $pagina." "; ?></b>
						<?php
								}
								else{
									//si el índice no corresponde con la página mostrada actualmente, coloco el enlace para ir a esa página 
						?>
									<a href="index.php?pagina=<?php echo $i ?>"><?php echo "$i"; ?></a>

						<?php
								}
							}
						}
					?>
				</article>
			</section>
			<footer>
				<article class="flxfoot margen">
					<article id="ar1" class="colmg">
						<h2 id="hGG">Calzado</h2>
					</article>
					<article class="colmg">
						<h2>Contacto</h2>
						<div><b>Cúcuta, Colombia</b></div>
						<div>hello@miweb.com</div>
						<div>+57 314 5678965</div>
						<div class="redes">
							<a href="" target="_blank"><span class="icon-facebook5"></span></a>
							<a href="" target="_blank"><span class="icon-twitter"></span></a>
							<a href="" target="_blank"><span class="icon-instagram"></span></a>
						</div>
					</article>
					<article class="colmg">
						<h2>Comprar Seguro</h2>
						<div class="flpag">
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
						</div>
						<div id="termcod">
							<b><a href="terminos">Términos</a> y <a href="condiciones">condiciones</a></b>
						</div>
					</article>
				</article>
			</footer>
		</section>
	</section>
	<script src="../../../js/prodadm.js"></script>
	<script src="../../../js/chosen_min.js"></script>
	<script type="text/javascript">
		//funciona sin ocultar el div
		$(".chosen-select").chosen();
		$("#dbb").chosen({disable_search_threshold: 10});
	</script>
</body>
</html>
<?php
	}
	else{
?>
<script type="text/javascript">
	window.location.href="../../erroradm.html"
</script>
<?php
	}
?>