<?php
	include '../../../config.php';
	session_start();
	if (isset($_SESSION['adm'])) {
		$iadR = $_SESSION['adm'];
		$sqadm = "SELECT * from administrador where id_adm=$iadR";
		$readm = $conexion->query($sqadm) or die ('Error sql');
		while ($ad = $readm->fetch_assoc()) {
			$idad = $ad['id_adm'];
			$usad = $ad['user_adm'];
			$tpad = $ad['tp_adm'];
			$esad = $ad['es_adm'];
		}
		//num_rows
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<meta name="description" content="Ingreso adm" />
	<title>Clientes |Productos |Zapatos</title>
	<link rel="stylesheet" href="../../../css/normalize.css" />
	<link rel="stylesheet" href="../../../css/iconos/style.css" />
	<link rel="stylesheet" href="../../../css/loadingw8.css" />
	<link rel="stylesheet" href="../../../css/style.css" />
	<link rel="stylesheet" href="../../../css/styadm.css" />
	<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="../../../js/scrippag.js"></script>
	<script src="../../../js/scripadm.js"></script>
</head>
<body>
	<section class="container">
		<nav id="mnV" class="mnnv">
			<a href="../">Inicio</a>
			<a href="../noticias">Noticias</a>
			<a href="../productos">Productos</a>
			<a href="../usuarios">Usuarios</a>
			<a href="../">Contabilidad</a>
		</nav>
		<header class="hea">
			<figure>
				<img src="../../../images/head1.jpg" alt="header1" />
			</figure>
		</header>
		<section>
			<div class="hoz mn1">
				<h2>Tipo de clientes</h2>
			</div>
			<nav id="mnP">
				<nav>
					<a href="../">Inicio</a>
					<a href="../noticias">Noticias</a>
					<a href="../productos">Productos</a>
					<a href="../usuarios">Usuarios</a>
					<a href="../">Contabilidad</a>
				</nav>
				<div id="btn_menu">
					<span class="icon-menu"></span>
				</div>
				<div id="carus">
					<div id="admus">
						<a href="../"><?php echo "$usad"; ?></a>
						<a href="../../../cerrar">Salir</a>
					</div>
				</div>
			</nav>
			<nav id="mnS">
				<a href="../productos">Ver Productos</a>
				<a href="cliente.php">Cliente</a>
				<a href="tipos.php">Tipos</a>
				<a href="marcas.php">Marcas</a>
				<a href="imagen_pP.php">Imagen Producto</a>
			</nav>
			<article class="margen">
				<form action="#" method="post" class="columninput">
					<h2>Nuevo Cliente</h2>
					<input type="text" id="ttcl" name="ttcl" required />
					<div id="txA"></div>
					<input type="submit" value="Ingresar" id="Nvcl" />
				</form>
			</article>
			<section class="sg4">
				<h1>Cleitnes Productos</h1>
				<article class="margen sg2 flxB">
					<?php
						error_reporting(E_ALL ^ E_NOTICE);
						$tamno_pagina=15;
						$pagina= $_GET['pagina'];
						if (!$pagina) {
							$inicio=0;
							$pagina=1;
						}
						else{
							$inicio= ($pagina - 1)*$tamno_pagina;
						}
						$ssql="SELECT * from clientep order by id_cl desc";
						$rs=$conexion->query($ssql) or die (mysqli_error());
						$num_total_registros= $rs->num_rows;
						$total_paginas= ceil($num_total_registros / $tamno_pagina);
						$gsql="SELECT * from clientep order by id_cl desc limit $inicio, $tamno_pagina";
						$impsql=$conexion->query($gsql) or die (mysqli_error());
						while ($gh=$impsql->fetch_assoc()) {
							$idCl = $gh['id_cl'];
							$ttCl = $gh['tt_cl'];
					?>
					<article id="cjac<?php echo $idCl ?>" class="fcaj">
						<div class="columninput columcent">
							<input type="text" id="ftcl_<?php echo $idCl ?>" value="<?php echo $ttCl ?>" required />
							<div id="txC_<?php echo $idCl ?>"></div>
							<input type="submit" value="Modificar" class="camcl" data-id="<?php echo $idCl ?>" />
							<a class="doll" href="borr_cliente.php" data-id="<?php echo $idCl ?>">Borrar</a>
						</div>
					</article>
					<?php
						}
					?>
				</article>
				<article class="margen sg2 ">
					<br />
					<b>Páginas: </b>
					<?php
						//muestro los distintos indices de las paginas
						if ($total_paginas>1) {
							for ($i=1; $i <=$total_paginas ; $i++) { 
								if ($pagina==$i) {
									//si muestro el indice del la pagina actual, no coloco enlace
						?>
							<b><?php echo $pagina." "; ?></b>
						<?php
								}
								else{
									//si el índice no corresponde con la página mostrada actualmente, coloco el enlace para ir a esa página 
						?>
									<a href="cliente.php?pagina=<?php echo $i ?>"><?php echo "$i"; ?></a>

						<?php
								}
							}
						}
					?>
				</article>
			</section>
			<footer>
				<article class="flxfoot margen">
					<article id="ar1" class="colmg">
						<h2 id="hGG">Calzado</h2>
					</article>
					<article class="colmg">
						<h2>Contacto</h2>
						<div><b>Cúcuta, Colombia</b></div>
						<div>hello@miweb.com</div>
						<div>+57 314 5678965</div>
						<div class="redes">
							<a href="" target="_blank"><span class="icon-facebook5"></span></a>
							<a href="" target="_blank"><span class="icon-twitter"></span></a>
							<a href="" target="_blank"><span class="icon-instagram"></span></a>
						</div>
					</article>
					<article class="colmg">
						<h2>Comprar Seguro</h2>
						<div class="flpag">
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
							<figure>
								<img src="" alt="" />
							</figure>
						</div>
						<div id="termcod">
							<b><a href="terminos">Términos</a> y <a href="condiciones">condiciones</a></b>
						</div>
					</article>
				</article>
			</footer>
		</section>
	</section>
	<script src="../../../js/prodadm.js"></script>
</body>
</html>
<?php
	}
	else{
?>
<script type="text/javascript">
	window.location.href="../../erroradm.html"
</script>
<?php
	}
?>